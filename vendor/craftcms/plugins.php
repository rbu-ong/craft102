<?php

$vendorDir = dirname(__DIR__);

return array (
  'products' => 
  array (
    'class' => 'craft\\products\\Plugin',
    'basePath' => $vendorDir . '/products/src',
    'handle' => 'products',
    'aliases' => 
    array (
      '@craft/products' => $vendorDir . '/products/src',
    ),
    'name' => 'Products',
    'version' => '1.0.0',
    'description' => 'A Craft CMS  EEWeb company plugin.',
    'developer' => 'AspenCoreDev Ultimates',
    'developerUrl' => 'https://bitbucket.org/ac-ultimates',
    'documentationUrl' => 'https://goo.gl/RFJnim',
    'hasCpSection' => true,
  ),
  'tagmanager' => 
  array (
    'class' => 'craft\\tagmanager\\Plugin',
    'basePath' => $vendorDir . '/tagmanager/src',
    'handle' => 'tagmanager',
    'aliases' => 
    array (
      '@craft/tagmanager' => $vendorDir . '/tagmanager/src',
    ),
    'name' => 'Tag Manager',
    'version' => '1.0.0',
    'description' => 'A Craft CMS  EEWeb company plugin.',
    'developer' => 'AspenCoreDev Ultimates',
    'developerUrl' => 'https://bitbucket.org/ac-ultimates',
    'documentationUrl' => 'https://goo.gl/RFJnim',
    'hasCpSection' => true,
  ),
  'accompanies' => 
  array (
    'class' => 'craft\\accompanies\\Plugin',
    'basePath' => $vendorDir . '/accompanies/src',
    'handle' => 'accompanies',
    'aliases' => 
    array (
      '@craft/accompanies' => $vendorDir . '/accompanies/src',
    ),
    'name' => 'Companies',
    'version' => '1.0.0',
    'description' => 'A Craft CMS  EEWeb company plugin.',
    'developer' => 'AspenCoreDev Ultimates',
    'developerUrl' => 'https://bitbucket.org/ac-ultimates',
    'documentationUrl' => 'https://goo.gl/RFJnim',
    'hasCpSection' => true,
  ),
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '1.0.1',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/redactor',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/redactor/master/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/redactor/archive/master.zip',
  ),
);
